
CREATE NONCLUSTERED INDEX IDXN_RPT_CitwebUsersWithoutEmails_FKBATCHID
ON [ex].[RPT_CitwebUsersWithoutEmails] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[UserId],[FullName],[Email],[ExceptionName],[ScriptAction])
GO
--
CREATE NONCLUSTERED INDEX IDXN_RPT_DuplicateSites_BATCHID
ON [ex].[RPT_DuplicateSites] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[SiteName],[SitePostCode],[CitwebSiteId],[SFDCAccountId],[SFDCSiteId],[ExecptionName])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_DuplicateCitwebUsers_FKBATCHID
ON [ex].[RPT_DuplicateCitwebUsers] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[UserId],[FullName],[Email],[ExceptionName],[ScriptAction])
GO
--
CREATE NONCLUSTERED INDEX IDXN_RPT_EmployeeCCPCExceptions_FKBATCHID
ON [ex].[RPT_EmployeeCCPCExceptions] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[EmployeeId],[FirstName],[MiddleName],[SecondName],[Email],[CitwebCountry],[AtlasCountry],[CitwebCounty],[AtlasCounty],[PostCode],[LeftCompany],[IsDisabled],[CountryExceptionName],[CountryTransformationAction],[CountyExceptionName],[CountyTransformationAction],[PostCodeExceptionName],[PostCodeTransformationAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_EmployeeDuplicateAbsenses_FKBATCHID
ON [ex].[RPT_EmployeeDuplicateAbsenses] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[EmployeeId],[FirstName],[MiddleName],[SecondName],[Email],[AbsenseUId],[DateFrom],[DateTo],[LeftCompany],[IsDisabled],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_EmployeeMultipleActiveJobs_FKBATCHID
ON [ex].[RPT_EmployeeMultipleActiveJobs] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[EmployeeId],[FirstName],[MiddleName],[SecondName],[Email],[JobUID],[Post],[DateStarted],[DateFinished],[LeftCompany],[IsDisabled],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_EmployeeMultipleAddresses_FKBATCHID
ON [ex].[RPT_EmployeeMultipleAddresses] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[EmployeeId],[FirstName],[MiddleName],[SecondName],[AddressRowNumber],[ContactId],[Email],[Address1],[Address2],[Town],[Country],[County],[PostCode],[Telephone],[Mobile],[LeftCompany],[IsDisabled],[ValidEmailCount],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_EmployeeNoActiveJob_FKBATCHID
ON [ex].[RPT_EmployeeNoActiveJob] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[EmployeeId],[FirstName],[MiddleName],[SecondName],[Email],[LeftCompany],[IsDisabled],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_EmployeeNullOrEmptyEmailId_FKBATCHID
ON [ex].[RPT_EmployeeNullOrEmptyEmailId] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[EmployeeId],[FirstName],[MiddleName],[SecondName],[Email],[LeftCompany],[IsDisabled],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_EmployeesWithSameEmail_FKBATCHID
ON [ex].[RPT_EmployeesWithSameEmail] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[EmployeeId],[FirstName],[MiddleName],[SecondName],[Email],[LeftCompany],[IsDisabled],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_InvalidCitwebStatus_FKBATCHID
ON [ex].[RPT_InvalidCitwebStatus] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[SFDCAccountId],[IsActiveInSalesForce],[IsEnabled],[IsCitationDemo],[IsTrailuser],[IsDeleted],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_InvalidEmailFormat_FKBATCHID
ON [ex].[RPT_InvalidEmailFormat] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[ObjectType],[ObjectId],[FullName],[Email],[EmployeeLeftCompany],[EmployeeDisabled],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_ManagerUserNotInEmployee_FKBATCHID
ON [ex].[RPT_ManagerUserNotInEmployee] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[UserId],[FullName],[Email],[IsManager],[IsHolidayAuthorisor],[IsDeleted],[Enabled],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_MissingDocuments_FKBATCHID
ON [ex].[RPT_MissingDocuments] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitwebSiteId],[SiteName],[DocumentObjectType],[DocumentObjectId],[DocumentFileName],[ExecptionName])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_SitesDiscrepencies_FKBATCHID
ON [ex].[RPT_SitesDiscrepencies] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitwebSiteId],[SFDCAccountId],[SFDCSiteId],[SiteName],[SitePostCode],[AtlasCompanyId],[AtlasSiteId],[ExecptionName])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_Summary_CREATEDON
ON [ex].[RPT_Summary] ([CreatedOn])
INCLUDE ([RowOrder],[SheetName],[Description],[RowCnt],[ClientsAffected],[SitesAffected],[EmployeesAffected],[UsersAffected])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_TrainerUsersDuplicateEmail_FKBATCHID
ON [ex].[RPT_TrainerUsersDuplicateEmail] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitWebSiteId],[CitWebSiteName],[UserId],[FirstName],[SurName],[Email],[IsActive],[ExceptionName],[ScriptAction])
GO
CREATE NONCLUSTERED INDEX IDXN_RPT_UnmappedCitwebSiteStats_FKBATCHID
ON [ex].[RPT_UnmappedCitwebSiteStats] ([FKBatchCompanyId])
INCLUDE ([FKBatchId],[CitWebCompanyId],[CitWebCompanyName],[CitwebSiteId],[SiteName],[EmployeeCount],[UsersCount],[TrainingUsersCount],[RiskAssessmentsCount],[CompanyDocumentsCount],[SiteVistDocumentsCount],[EmployeePicturesCount],[EmployeeDocumentsCount],[ExecptionName])
GO