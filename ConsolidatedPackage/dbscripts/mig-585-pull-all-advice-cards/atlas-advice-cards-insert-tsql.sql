-- Input Params
DECLARE @p_CompanyId			uniqueidentifier = ? -- 'CEBA4C29-E9B3-4F2F-A49F-8B3EBCD0A395'
DECLARE @p_Email				varchar(200)	 = ? -- 'nigel.fisher@gdsystems.com'
DECLARE @p_AdviceCardNumber		varchar(20)		 = ? -- '81097btoh'
DECLARE @p_ServiceType			varchar(10)		 = ? -- 'BOTH'
--
-- Local Variables
DECLARE @AtlasAdviceCardId		uniqueidentifier
DECLARE @UserId					uniqueidentifier
DECLARE @Now					smalldatetime			= getdate()
DECLARE @MappedServiceType		varchar(10)
--
-- Constants
DECLARE @c_DefaultUserId		uniqueidentifier = '89504E36-557B-4691-8F1B-7E86F9CF95EA'
--
SET @MappedServiceType = CASE WHEN @p_ServiceType = 'HS' THEN 'H&S' ELSE CASE WHEN @p_ServiceType = 'HR' THEN 'EL' ELSE @p_ServiceType END END
--
SELECT @AtlasAdviceCardId = Id FROM Citation.AdviceCards WHERE CompanyId = @p_CompanyId AND CardNumber = @p_AdviceCardNumber
--
SELECT @UserId = Id FROM Shared.Users u WHERE CompanyId = @p_CompanyId AND Email = @p_Email AND IsDeleted = 0 AND IsActive = 1
--
-- print @AtlasAdviceCardId
--
-- Get atlasadvicecard if exist or insert into Citation.AdviceCards and get the new id
IF @AtlasAdviceCardId IS NULL
BEGIN
	SET @AtlasAdviceCardId = NEWID()
	--
	INSERT INTO	Citation.AdviceCards (
				Id
				,CompanyId
				,CardNumber
				,CreatedOn
				,ModifiedOn
				,CreatedBy
				,ModifiedBy
				,IsArchived
				,IsDeleted
				,LCid
				,Version
				,IsActive) VALUES (
				 @AtlasAdviceCardId --Id
				,@p_CompanyId --CompanyId
				,@p_AdviceCardNumber --CardNumber
				,@Now --CreatedOn
				,@Now --ModifiedOn
				,@c_DefaultUserId --CreatedBy
				,@c_DefaultUserId --ModifiedBy
				,0 --IsArchived
				,0 --IsDeleted
				,1033 --LCid
				,'1.0' --Version
				,1 --IsActive
				)
--
END
-- insert into Citation.AdviceCardAssignments
IF @UserId IS NOT NULL AND NOT EXISTS (SELECT 'X' FROM Citation.AdviceCardAssignments WHERE UserId = @UserId AND AdviceCardId = @AtlasAdviceCardId)
INSERT INTO	Citation.AdviceCardAssignments (
			Id
			,UserId
			,AdviceCardId
			,CreatedOn
			,ModifiedOn
			,CreatedBy
			,ModifiedBy
			,IsDeleted
			,LCid
			,Version) VALUES (
			NEWID()--Id
			,@UserId --UserId
			,@AtlasAdviceCardId --AdviceCardId
			,@Now --CreatedOn
			,@Now --ModifiedOn
			,@c_DefaultUserId --CreatedBy
			,@c_DefaultUserId --ModifiedBy
			,0 --IsDeleted
			,1033 --LCid
			,'1.0' --Version
			)
--
-- Insert Citation.AdviceCardTypeMap if not exist for that card (won't insert anything if service type changes)
INSERT INTO	Citation.AdviceCardTypeMap (
			AdviceCardId
			,AdviceTypeId
			)
SELECT		@AtlasAdviceCardId
			,at.Id
FROM		Shared.AdviceTypes at
WHERE		(((@MappedServiceType = 'H&S' OR @MappedServiceType = 'BOTH') AND NAME = 'H&S')
		OR	((@MappedServiceType = 'EL' OR @MappedServiceType = 'BOTH') AND NAME = 'EL'))
AND	NOT EXISTS (SELECT 1 FROM Citation.AdviceCardTypeMap act WHERE act.AdviceCardId =  @AtlasAdviceCardId)
