DECLARE @p_SFDCAccountId			nvarchar(36)		= ? -- '001D000001bYgmeIAC'
DECLARE @p_CaseType					varchar(100)		= ? -- 'In-bound'
DECLARE @p_Campagin					varchar(100)		= ? -- '200 Trial'
DECLARE	@p_AtlasSiteURL				varchar(100)		= ? -- 'https://atlasdatamig-dev.azurewebsites.net'
--
DECLARE @NewIssueId					int					= 0
DECLARE @AtlasCompanyId				uniqueidentifier
DECLARE @ServiceOwenerUserId		uniqueidentifier
DECLARE @JitBitUserId				int
DECLARE @CompanyName				nvarchar(1000)
DECLARE @Segment					nvarchar(20)
DECLARE @ServicesTaken				nvarchar(100)		= ''
DECLARE @Subject					nvarchar(2000)
DECLARE @Body						nvarchar(4000)
DECLARE @CaseTypeId					int
DECLARE @CaseTypeOptionId			int
DECLARE @CampaignId					int
DECLARE @CampaignOptionId			int
DECLARE	@ErrorMsg					varchar(2000)
--
DECLARE @C_NewLine					char(1) = char(10)
DECLARE @C_NullGUID					nvarchar(36) =		'00000000-0000-0000-0000-000000000000'
-- Get Company Name, Segment, Main Site Id, PostCode, TenantName
SELECT		@AtlasCompanyId = c.Id, @CompanyName = c.FullName, @Segment = cs.Title
FROM		citation.Companies c
LEFT JOIN	Citation.ClientSegments cs
ON			c.ClientSegmentId	= cs.Id
WHERE		c.SalesforceAccountID = @p_SFDCAccountId
AND			c.IsDeleted			= 0
AND			c.IsActive			= 1
--
IF (@AtlasCompanyId	IS NULL)
BEGIN
	SET @ErrorMsg			= 'Could not find company for SFDC Account Id ' + @p_SFDCAccountId
	SET @AtlasCompanyId		= @C_NullGUID
	--RAISERROR(@ErrorMsg, 16, 1)
	GOTO ENDOFSCRIPT
END
-- Get Services Taken
SELECT		@ServicesTaken = @ServicesTaken + ClientServiceType + ','
FROM		(
SELECT		DISTINCT c.Id CompanyId, c.FullName,
			CASE WHEN cst.CitationServiceType = 1 THEN 'H&S' ELSE
					CASE WHEN cst.CitationServiceType = 2 THEN 'PEL' ELSE
							CASE WHEN cst.CitationServiceType = 3 THEN 'ISO' ELSE 'UNKNOWN' END END END ClientServiceType
FROM		Citation.Companies c
INNER JOIN	Citation.CompanyClientTypes cct
ON			cct.CompanyId		= c.Id
INNER JOIN	Shared.CitationServiceClientType cst
ON			cst.clienttypeid	= cct.ClientTypeId
WHERE		c.Id				= @AtlasCompanyId
) t
--

IF ISNULL(@ServicesTaken,'') <> ''
	SET @ServicesTaken = SUBSTRING(@ServicesTaken,1,LEN(@ServicesTaken)-1)
--
--
-- Get one of multiple service owners (active user gets priority)
SELECT		TOP 1 @ServiceOwenerUserId = u.Id, @JitBitUserId = JitbitUserId
FROM		shared.Users u
WHERE		u.CompanyId			= @AtlasCompanyId
AND			u.IsDeleted			= 0
AND			u.IsActive			= 1
AND	EXISTS (SELECT 1
			FROM		UserProfilesMap upm
			WHERE		upm.UserId = u.id
			AND			upm.UserProfileId IN ('1078A9A4-101E-4B22-AF96-446AA7C5C793','ED8F7D9B-DFFD-49EC-8934-5327D2BF1AA4','EFD4D143-48C1-471C-B8BA-B91ADBC9B156'))
AND			u.IsDeleted			= 0
ORDER BY	u.IsActive, u.JitbitUserId DESC
--
-- Get UserId from incidents.hdUsers using email when jitbit is not available
--
IF (@JitBitUserId IS NULL)
	SELECT	TOP 1 @JitBitUserId = UserId FROM incidents.hdUsers WHERE Email = (SELECT Email FROM shared.Users WHERE Id = @ServiceOwenerUserId)
--
-- Build Subject
--
SET @Subject	= @CompanyName + ' - (' + ISNULL(@Segment,'') + ' / ' + ISNULL(@ServicesTaken,'') + ') - ' + @p_CaseType + ' - ' + FORMAT (getdate(), 'D', 'en-gb')
--
SET @Body = 'Warning!!! Ticket body will be overwritten after Migration. Please use ''Reply'' section instead'
--
IF @JitBitUserId IS NULL
	SET @JitBitUserId = 6795

--	SET @ErrorMsg = 'Unable to get UserId of Service Swner for the company ' + @CompanyName
--	RAISERROR(@ErrorMsg, 16, 1)
INSERT INTO incidents.hdIssues (
	InstanceID
	,IssueDate
	,UserID
	,CategoryID
	,Subject
	,Body
	,UpdatedByUser
	,UpdatedByPerformer
	,UpdatedForTechView
	,StatusID
	,PublishToKB
	,Priority
	,TimeSpentInSeconds
	,KBForTechsOnly
	,LastUpdated
	,ChannelId
	,IsDeleted
	,IsAdviceGuaranteed

) VALUES (
	0 -- InstanceID
	,GETDATE() -- IssueDate
	,@JitBitUserId -- UserID
	,(SELECT CategoryId FROM incidents.hdcategories WHERE Name = 'Migration') -- CategoryID
	,ISNULL(@Subject, 'SystemError: Something strange happend while creating pre-migartion case for SFDC account ' + @p_SFDCAccountId)-- Subject
	,@Body -- Body
	,0 -- UpdatedByUser
	,0 -- UpdatedByPerformer
	,0 -- UpdatedForTechView
	,1 -- StatusID
	,0 -- PublishToKB
	,0 -- Priority
	,0 -- TimeSpentInSeconds
	,0 -- KBForTechsOnly
	,GETDATE() -- LastUpdated
	,1 -- ChannelId
	,0 -- IsDeleted
	,0 -- IsAdviceGuaranteed
)
--
SET @NewIssueId = SCOPE_IDENTITY()
--
-- 
IF (LEN(@p_CaseType) > 0)
BEGIN
	--
	SELECT @CaseTypeId = FieldId FROM Incidents.hdCustomFields WHERE FieldName = 'Case Type'
	--
	IF (@CaseTypeId IS NOT NULL)
	BEGIN
		SELECT @CaseTypeOptionId = OptionId FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CaseTypeId AND OptionValue = @p_CaseType AND IsDeleted = 0
		-- Create option if not exists
		--
		IF (@CaseTypeOptionId IS NULL)
		BEGIN
			INSERT INTO Incidents.hdCustomFieldOptions (FieldId, OptionValue, IsDefault, IsDeleted, OrderByNumber)
			VALUES
			(@CaseTypeId, @p_CaseType, 0, 0, (SELECT MAX(OrderByNumber) + 1 FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CaseTypeId))
			--
			SET @CaseTypeOptionId = SCOPE_IDENTITY()
		END
		--
		INSERT INTO	Incidents.hdCustomFieldValues (IssueID, FieldID, Value) VALUES (@NewIssueId, @CaseTypeId, @CaseTypeOptionId)
		--
	END
END
--
IF (LEN(@p_Campagin) > 0)
BEGIN
	--
	SELECT @CampaignId = FieldId FROM Incidents.hdCustomFields WHERE FieldName = 'Campaign/Batch'
	--
	IF (@CampaignId IS NOT NULL)
	BEGIN
		SELECT @CampaignOptionId = OptionId FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CampaignId AND OptionValue = @p_Campagin AND IsDeleted = 0
		-- Create option if not exists
		--
		IF (@CampaignOptionId IS NULL)
		BEGIN
			INSERT INTO Incidents.hdCustomFieldOptions (FieldId, OptionValue, IsDefault, IsDeleted, OrderByNumber)
			VALUES
			(@CampaignId, @p_Campagin, 0, 0, (SELECT MAX(OrderByNumber) + 1 FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CampaignId))
			--
			SET @CampaignOptionId = SCOPE_IDENTITY()
		END
		--
		INSERT INTO	Incidents.hdCustomFieldValues (IssueID, FieldID, Value) VALUES (@NewIssueId, @CampaignId, @CampaignOptionId)
	END
END
--
ENDOFSCRIPT:
SELECT ? = @NewIssueId, ? = @AtlasCompanyId
--
-- SELECT @NewIssueId, @AtlasCompanyId