declare @p_IssueId int = 185556
declare @p_AtlasId uniqueidentifier = null -- optional
--
declare @atlasid nvarchar(36)
DECLARE @ServiceOwenerUserId		uniqueidentifier
DECLARE @JitBitUserId				int
DECLARE @CompanyName				nvarchar(1000)
DECLARE @CompaniesCount				int
-- DECLARE @Segment					nvarchar(20)
-- DECLARE @ServicesTaken				nvarchar(100) = ''
DECLARE @TenantName					nvarchar(100)
DECLARE @HQPostCode					nvarchar(100)
DECLARE @SitesCount					int
DECLARE @EmployeeCount				int
DECLARE @ConsultantName				nvarchar(200)
DECLARE @SOName						nvarchar(200)
DECLARE @SOEmail					nvarchar(200)
DECLARE @SOPhone					nvarchar(200)
declare @subject nvarchar(1000)
declare @body nvarchar(4000)
DECLARE @BPCompanyId			nvarchar(36)
DECLARE @BPDocumentId			nvarchar(36)
--
print 'Verifying IssueId: ' + cast(@p_IssueId as varchar(10))
--
if (@p_AtlasId is not null)
	set @atlasid = @p_AtlasId
else
begin
	print 'Atlas Id not given, trying to extract from incident subject'
	select @subject = subject, @CompanyName = substring(subject,1,charindex(' - (',subject) - 1) from incidents.hdissues where issueid = @p_IssueId
	print 'Trying to get atlas id from company name extracted ''' + @CompanyName + ''''
	SELECT @CompaniesCount = COUNT(*) FROM citation.companies where fullname = @CompanyName and isdeleted = 0

	if (@CompaniesCount <> 1)
	begin
		print 'Can''t continue further. Failed to get atlas id because we could not extract company name properly or there are multiple companies found with the same name. Please pass the id'
		goto ENDOFSCRIPT
	end

	select @atlasid = id from citation.companies where fullname = @CompanyName and  isdeleted = 0
	
	
end

select		@CompanyName = FullName, @HQPostCode = ISNULL(a.PostCode,''), @TenantName =ISNULL(TenantName,'')--, @JitBitUserId = JitBitUserId
FROM		Citation.Companies c
LEFT JOIN	dbo.Sites s
ON			s.CompanyId			= c.Id
AND			s.IsHeadOffice		= 1
LEFT JOIN	Addresses a
ON			s.AddressId			= a.Id
WHERE		c.Id				= @AtlasId
--
print 'Atlas Id: ' + @atlasid
print 'Company: ' + @CompanyName
--
print 'HQ Post Code: ' + @HQPostCode
print 'Tenant Name: ' + @TenantName
--

SELECT		TOP 1 @ServiceOwenerUserId = u.Id, @JitBitUserId = JitbitUserId, @SOName = ISNULL(u.FirstName,'') + ' ' + ISNULL(u.SecondName,''),
			@SOEmail = u.Email, @SOPhone = ISNULL(COALESCE(u.Telephone, u.MobileNumber),'')
FROM		shared.Users u
WHERE		u.CompanyId			= @AtlasId
AND			u.IsDeleted			= 0
AND			u.IsActive			= 1
AND	EXISTS (SELECT 1
			FROM		UserProfilesMap upm
			WHERE		upm.UserId = u.id
			AND			upm.UserProfileId IN ('1078A9A4-101E-4B22-AF96-446AA7C5C793','ED8F7D9B-DFFD-49EC-8934-5327D2BF1AA4','EFD4D143-48C1-471C-B8BA-B91ADBC9B156'))
AND			u.IsDeleted			= 0
ORDER BY	u.IsActive, u.JitbitUserId DESC
--
if (@ServiceOwenerUserId is null)
	print 'No SO found for this company'
else
begin
	print 'SO Id: ' + cast(@ServiceOwenerUserId as varchar(36))
	print 'SO Name: ' + @SOName
	print 'So Email: ' + @SOEmail
	print 'SO Phone: '+ @SOPhone
	if (@JitBitUserId IS NULL)
	begin
		print 'Jitbit id is null. Trying to get from hdUsers using SO user''s email'
		SELECT	TOP 1 @JitBitUserId = UserId FROM incidents.hdUsers WHERE Email = (SELECT Email FROM shared.Users WHERE Id = @ServiceOwenerUserId)
	end
end
print 'JitbitId: ' + isnull(cast(@JitBitUserId as varchar(10)),'Default UserId mapped' )
--
SELECT @SitesCount = COUNT(*) FROM dbo.Sites WHERE CompanyId = @AtlasId AND IsDeleted = 0 AND IsActive = 1
SELECT @EmployeeCount = COUNT(*) FROM dbo.Employees WHERE CompanyId = @AtlasId AND IsDeleted = 0 AND IsActive = 1

print 'Sites: ' + cast(@SitesCount as varchar (3))
print 'Employees: ' + cast(@EmployeeCount as varchar(4))

-- Get Consulttant
SELECT	@ConsultantName = ISNULL(u.FirstName,'') + ' ' + ISNULL(u.SecondName,'')
FROM	Shared.Users u
INNER JOIN Shared.SiteAssignments sa
ON		sa.UserId		= u.Id
INNER JOIN dbo.Sites s
ON		s.Id			= sa.SiteId
WHERE	s.CompanyId		= @AtlasId
AND		s.IsHeadOffice	= 1
AND		s.IsDeleted		= 0
AND		u.IsActive		= 1
AND		u.IsDeleted		= 0
--
print 'Consultant: ' + ISNULL(@ConsultantName,'Not found')
select --substring(subject,1,charindex(' - (',subject) - 1) CompanyName
	@BPDocumentId = substring(body,charindex('#/document/',body) + 11,36)
	, @BPCompanyId = substring(body,charindex('?cid=',body) + 5,36)
from incidents.hdissues
where issueid = @p_issueId
and categoryid = 25
and body like 'Migrated on:%'
print 'Company and Document Ids in Body: ' + @BPCompanyId + ', ' + @BPDocumentId
--
ENDOFSCRIPT:
print 'Done!'
--
-- select subject = subject, CompanyName = substring(subject,1,charindex(' - (',subject) - 1) from incidents.hdissues where issueid = 185218
