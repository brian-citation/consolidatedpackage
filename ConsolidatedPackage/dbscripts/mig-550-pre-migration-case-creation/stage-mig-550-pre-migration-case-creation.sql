-- DROP TABLE [dbo].[Stage_Incidents_Track]
CREATE TABLE [dbo].[Stage_Incidents_Track](
	[AtlasIncidentId]		[int]				NOT NULL	PRIMARY KEY,
	[CitwebCompanyId]		[int]				NOT NULL,
	[AtlasCompanyId]		[uniqueidentifier]	NOT NULL,
	[SFDCAccountId]			[nvarchar](36)		NOT NULL,
	[CreatedOn]				[datetime]			NOT NULL	DEFAULT getdate(),
	[IsDeleted]				[bit]				NOT NULL	DEFAULT 0,

)
