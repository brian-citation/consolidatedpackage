DECLARE @p_IssueId					int					= ? -- 183507
DECLARE @p_AtlasCompanyId			uniqueidentifier	= ? -- 'E10C5B1E-D5FD-43C5-8E82-B060F38A53AB'
DECLARE @p_AtlasDocumentId			uniqueidentifier	= ? -- 'aaaaaaaa-c5ed-48c1-97e1-62c934332d47'
DECLARE @p_CitwebLastLogin			varchar(100)		= ? -- FORMAT (getdate(), 'D', 'en-gb')
DECLARE	@p_AtlasSiteURL				varchar(100)		= ? -- 'https://atlasdatamig-dev.azurewebsites.net'
--
DECLARE @ServiceOwenerUserId		uniqueidentifier
DECLARE @JitBitUserId				int
DECLARE @CompanyName				nvarchar(1000)
DECLARE @TenantName					nvarchar(100)
DECLARE @HQSiteId					uniqueidentifier
DECLARE @HQPostCode					nvarchar(100)
DECLARE @SitesCount					int
DECLARE @EmployeeCount				int
DECLARE @ConsultantName				nvarchar(200)
DECLARE @SOName						nvarchar(200)
DECLARE @SOEmail					nvarchar(200)
DECLARE @SOPhone					nvarchar(200)
DECLARE @Subject					nvarchar(2000)
DECLARE @Body						nvarchar(4000)
DECLARE @CaseTypeId					int
DECLARE @CaseTypeOptionId			int
DECLARE @CampaignId					int
DECLARE @CampaignOptionId			int
DECLARE	@ErrorMsg					varchar(2000)
--
DECLARE @C_NewLine					char(1) = char(10)
DECLARE @C_DefaultJitBitUserId		int = 6795
--
IF NOT EXISTS (SELECT 'X' FROM Incidents.hdIssues WHERE IssueId = @p_IssueId)
BEGIN
	SET @ErrorMsg = 'Invalid Case Id given. Case Id:' + CAST(@p_IssueId AS VARCHAR(10))
	RAISERROR(@ErrorMsg, 16, 1)
END
--
-- Get Company Name, Segment, Main Site Id, PostCode, TenantName
-- @Segment = cs.Title, 
SELECT		@CompanyName = c.FullName, @HQSiteId = s.Id, @HQPostCode = a.PostCode, @TenantName = c.TenantName
FROM		citation.Companies c
LEFT JOIN	dbo.Sites s
ON			s.CompanyId			= c.Id
AND			s.IsHeadOffice		= 1
LEFT JOIN	Citation.ClientSegments cs
ON			c.ClientSegmentId	= cs.Id
LEFT JOIN	Addresses a
ON			s.AddressId			= a.Id
WHERE		c.Id				= @p_AtlasCompanyId
--AND			s.IsDeleted			= 0 -- Removed this case as we don't want to stop whole process for some weired issue

--
IF	(@CompanyName IS NULL)
BEGIN
	SET @ErrorMsg = 'Unexpected error occured while updating Issue Id ' + CAST(@p_IssueId AS VARCHAR(10)) + ' . Atlas Id ' + CAST(@p_AtlasCompanyId AS VARCHAR(36)) + ' is invalid or company not active or head office not found'
	RAISERROR(@ErrorMsg, 16, 1)
END
--
-- Site Count
SELECT @SitesCount = COUNT(*) FROM dbo.Sites WHERE CompanyId = @p_AtlasCompanyId AND IsDeleted = 0 AND IsActive = 1
-- Empoloyee Count
SELECT @EmployeeCount = COUNT(*) FROM dbo.Employees WHERE CompanyId = @p_AtlasCompanyId AND IsDeleted = 0 AND IsActive = 1
--
-- Get one of multiple service owners (active user gets priority)
SELECT		TOP 1 @ServiceOwenerUserId = u.Id, @JitBitUserId = JitbitUserId, @SOName = ISNULL(u.FirstName,'') + ' ' + ISNULL(u.SecondName,''),
			@SOEmail = u.Email, @SOPhone = ISNULL(COALESCE(u.Telephone, u.MobileNumber),'')
FROM		shared.Users u
WHERE		u.CompanyId			= @p_AtlasCompanyId
AND			u.IsDeleted			= 0
AND			u.IsActive			= 1
AND	EXISTS (SELECT 1
			FROM		UserProfilesMap upm
			WHERE		upm.UserId = u.id
			AND			upm.UserProfileId IN ('1078A9A4-101E-4B22-AF96-446AA7C5C793','ED8F7D9B-DFFD-49EC-8934-5327D2BF1AA4','EFD4D143-48C1-471C-B8BA-B91ADBC9B156'))
AND			u.IsDeleted			= 0
ORDER BY	u.IsActive, u.JitbitUserId DESC
--
-- Get UserId from incidents.hdUsers using email when jitbit is not available
--
IF (@JitBitUserId IS NULL)
	SELECT	TOP 1 @JitBitUserId = UserId FROM incidents.hdUsers WHERE Email = (SELECT Email FROM shared.Users WHERE Id = @ServiceOwenerUserId)
--
-- Get Consulttant
SELECT	@ConsultantName = ISNULL(u.FirstName,'') + ' ' + ISNULL(u.SecondName,'')
FROM	Shared.Users u
INNER JOIN Shared.SiteAssignments sa
ON		sa.UserId		= u.Id
INNER JOIN dbo.Sites s
ON		s.Id			= sa.SiteId
WHERE	s.CompanyId		= @p_AtlasCompanyId
AND		s.IsHeadOffice	= 1
AND		s.IsDeleted		= 0
AND		u.IsActive		= 1
AND		u.IsDeleted		= 0
--
SET @Body = 'Migrated on: ' + FORMAT (getdate(), 'D', 'en-gb') + @C_NewLine +
			@CompanyName + ', ' + ISNULL(@HQPostCode,'') + @C_NewLine +
			'Tota Sites: ' + CAST(@SitesCount as varchar(3)) + ' Total Employees: ' + CAST(@EmployeeCount AS VARCHAR(5)) + @C_NewLine +
			'Consultant: ' + ISNULL(@ConsultantName, '') + @C_NewLine +
			'Last CitWeb log-in date: ' + @p_CitwebLastLogin + @C_NewLine + @C_NewLine +
			'Service Owner:  ' + ISNULL(@SOName,'') + ', email: ' + ISNULL(@SOEmail,'') + ', tel: ' + ISNULL(@SOPhone,'') + @C_NewLine +
			'Exception report: ' + @p_AtlasSiteURL + '/#/document/' + CAST(@p_AtlasDocumentId AS VARCHAR(36)) + '?cid=' + CAST(@p_AtlasCompanyId AS VARCHAR(36)) + @C_NewLine +
			'Company details page: ' + @p_AtlasSiteURL + '/#/company/' + CAST(@p_AtlasCompanyId AS VARCHAR(36)) + '?cid=' + CAST(@p_AtlasCompanyId AS VARCHAR(36))
--
--
UPDATE Incidents.hdIssues SET UserID = ISNULL(@JitBitUserId, @C_DefaultJitBitUserId) , Body = @Body WHERE IssueId = @p_IssueId
--
SELECT ? = @TenantName
--
-- SELECT @TenantName
