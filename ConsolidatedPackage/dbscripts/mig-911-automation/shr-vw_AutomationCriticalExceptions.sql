if exists (select * from sys.views where name = 'vw_AutomationCriticalExceptions')
	drop view dbo.vw_AutomationCriticalExceptions
go

create view dbo.vw_AutomationCriticalExceptions as
	
	SELECT		--SalesForceAccountId, CompanyName, ExceptionName, AtlasCompanyId, CitWebCompanyId
	*
	from		dbo.ExceptionsConsolidated 
	where		Severity = 5
	and			ExceptionType in ('DuplicateSites', 'SitesDiscrepencies', 'ClientServiceTypeUndefined', )
	--
	select * from exceptions where isactive = 1 and Severity = 5
	--

