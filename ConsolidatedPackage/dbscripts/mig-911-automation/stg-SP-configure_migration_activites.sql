IF EXISTS (select * from sys.procedures where name  = 'Configure_Migration_Activities')
	DROP PROCEDURE dbo.Configure_Migration_Activities
GO
CREATE PROCEDURE dbo.Configure_Migration_Activities as 
begin
	--
	DECLARE @ConfigurationTime datetime = getdate()
	-- force unfinished activites as completed if some clients are removed from sfdc
	UPDATE		cma
	SET			IsProcessed			= -1
				, ReferenceId		= 'SP Configure_Migration_Activities removed entry after client gone missing from schedule'
	FROM		dbo.Current_Migration_Activity cma
	WHERE		IsProcessed			= 0
	AND NOT EXISTS (SELECT * FROM dbo.Stage_SFDC_Schedule sss WHERE sss.IsDeleted = 0 AND sss.SalesforceAccountId = cma.SalesforceAccountId)
	--
	-- Critical exception email (14 days before migration date)
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		1 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Scheduled'
	AND			DaysDifference			BETWEEN 1 AND 14 
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 1 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	-- Case creation (7 days before before migration date)
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		2 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Scheduled'
	AND			DaysDifference			BETWEEN 1 AND 7
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 2 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	-- Certificate generation (1 day before migration date)
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		3 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Scheduled'
	AND			DaysDifference			=  1
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 3 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	-- Copy FTF files
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		4 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Scheduled'
	AND			DaysDifference			=  1
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 4 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	-- Excecute Exception reports
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		5 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Scheduled'
	AND			DaysDifference			=  1
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 5 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	--  Force activity as completed for the clients whose exceptions report was not generated)
	UPDATE		cma
	SET			IsProcessed				= 1,
				ReferenceId				= 'Forcing IsProcessed = 1 because exception reports did not run for this client',
				UpdatedOn				= getdate()
	FROM		dbo.Current_Migration_Activity cma
	WHERE		cma.ActivityId			= 6
	AND			cma.IsProcessed			= 0
	AND	EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma1 Where cma.ActivityId = 5 AND IsProcessed = 0 AND cma1.SalesforceAccountId = cma.SalesforceAccountId)
	--
	-- Excecute Migration scripts
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		6 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Scheduled'
	AND			DaysDifference			= 0
	AND			HaveCriticalExceptions	= 0
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 6 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	-- Token reset on 7th day after migration
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		7 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Complete'
	AND			DaysDifference			=  -7
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 7 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	-- Token reset on 14th day after migration
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		8 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Complete'
	AND			DaysDifference			=  -14
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 8 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
	--
	-- Token reset on 21th day after migration
	INSERT INTO dbo.Current_Migration_Activity (ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId)
	SELECT		9 ActivityId, SalesforceAccountId, CompanyName, CitwebCompanyId, AtlasCompanyId
	FROM		dbo.Stage_SFDC_Schedule sfs
	WHERE		IsDeleted				= 0
	AND			MigrationStatus			= 'Complete'
	AND			DaysDifference			=  -21
	AND NOT EXISTS (SELECT * FROM dbo.Current_Migration_Activity cma Where cma.ActivityId = 9 AND sfs.SalesforceAccountId = cma.SalesforceAccountId AND cma.IsProcessed <> -1)
end