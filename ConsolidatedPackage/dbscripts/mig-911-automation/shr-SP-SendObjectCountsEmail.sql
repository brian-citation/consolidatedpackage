-- exec dbo.SendObjectsCountsEmail 'D:\ExceptionReports\input\MigrationObjectCounts20170105.xls'
if exists (select * from sys.procedures where name = 'SendObjectsCountsEmail')
	drop procedure dbo.SendObjectsCountsEmail
go

CREATE PROCEDURE dbo.SendObjectsCountsEmail (@p_FilePath nvarchar(500)) as
begin
	declare @EmailSubject nvarchar(100) = 'Client(s) migrated to Atlas!!!'
	declare @EmailBody nvarchar(max) = 'Hello, this it to inform you that tehnical migration is completed for the clients scheduled today.' + replicate(char(13),2) + 
								'This is automatic email generated on ' + cast( getdate() as nvarchar(50)) + replicate(char(13),2) + 
								'Regards,' + char(13) + 'DM Team'

	declare @FilePath nvarchar(500) = @p_FilePath
	--
	exec msdb.dbo.sp_send_dbmail
	  @profile_name ='Emailer',
	  @recipients='devenderbejju@citation.co.uk;ramachandra.pendyala@valuelabs.com;hari.kuricheti@valuelabs.com',
	  @subject=@EmailSubject,
	  @body=@EmailBody, 
	  @file_attachments=@FilePath
end
go

-- select replicate('abc',4)