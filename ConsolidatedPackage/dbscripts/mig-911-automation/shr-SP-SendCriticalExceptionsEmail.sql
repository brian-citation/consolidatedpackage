exec dbo.SendCriticalExceptionsEmail 'D:\ExceptionReports\output\51_MultipleCompanies_ExceptionReport.xls'
if exists (select * from sys.procedures where name = 'SendCriticalExceptionsEmail')
	drop procedure dbo.SendCriticalExceptionsEmail
go

CREATE PROCEDURE dbo.SendCriticalExceptionsEmail (@p_FilePath nvarchar(500)) as
begin
	declare @EmailSubject nvarchar(100) = 'Data Migration: Critical Exceptions Alert!'
	declare @EmailBody nvarchar(max) = 'This it to inform you that one or more clients scheduled for migration have critical excpetions which require manual cleansing.' + char(13) + 
								'This is automatic email generated on ' + cast( getdate() as nvarchar(50))
	declare @FilePath nvarchar(500) = @p_FilePath
	--
	exec msdb.dbo.sp_send_dbmail
	  @profile_name ='Emailer',
	  @recipients='devenderbejju@citation.co.uk;ramachandra.pendyala@valuelabs.com;hari.kuricheti@valuelabs.com',
	  @subject=@EmailSubject,
	  @body=@EmailBody, 
	  @file_attachments=@FilePath
end
go