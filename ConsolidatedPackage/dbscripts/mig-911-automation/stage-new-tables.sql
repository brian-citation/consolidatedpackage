IF EXISTS (SELECT * FROM sys.tables WHERE NAME = 'Stage_SFDC_Schedule')
	DROP TABLE dbo.Stage_SFDC_Schedule
GO
--
CREATE TABLE dbo.Stage_SFDC_Schedule (
	SFScheduleId				int					not null primary key identity,
	SalesforceAccountId			nvarchar(36)		not null,
	CompanyName					nvarchar(510)		not null,
	CitwebCompanyId				int					not null,
	AtlasCompanyId				uniqueidentifier	null,
	MigrationDate				datetime			not null,
	DaysDifference				as datediff(d, cast(getdate() as date), cast(MigrationDate as date)),
	MigrationStatus				nvarchar (500)		not null,
	Migrated					int					not null,
	MigrationPhaseCategory		nvarchar(510)		null,
	MigrationTrialGroup			nvarchar(510)		null,
	HaveCriticalExceptions		bit					not null default (0),
	IsDeleted					int					not null default (0),
	CreatedOn					datetime			not null default (getdate()),
	UpdatedOn					datetime			not null default (getdate())
)
GO
--
--
IF EXISTS (SELECT * FROM sys.tables WHERE NAME = 'Migration_Activities')
	DROP TABLE dbo.Migration_Activities
GO
--
CREATE TABLE dbo.Migration_Activities(
	ActivityId					int					not null primary key identity,
	ActivityName				nvarchar(100)		not null unique
)
--
GO
--
IF EXISTS (SELECT * FROM sys.tables WHERE NAME = 'Current_Migration_Activity')
	DROP TABLE dbo.Current_Migration_Activity
GO
--
CREATE TABLE dbo.Current_Migration_Activity (
	Id							int					not null identity,
	SalesforceAccountId			nvarchar(36)		not null,
	ActivityId					int					not null,
	CompanyName					nvarchar(510)		not null,
	CitwebCompanyId				int					not null,
	AtlasCompanyId				uniqueidentifier	null,
	IsProcessed					int					not null default (0),
	ReferenceId					nvarchar(510)		null,
	CreatedOn					datetime			not null default (getdate()),
	UpdatedOn					datetime			not null default (getdate())

)

GO
---- NOT REQUIRED (ON HOLD)
--IF EXISTS (SELECT * FROM sys.tables WHERE NAME = 'ATLAS_USER_IN_MULTIPLE_Companies')
--	DROP TABLE dbo.ATLAS_USER_IN_MULTIPLE_Companies
--GO
----
--CREATE TABLE ATLAS_USER_IN_MULTIPLE_Companies (
--	Id							int					not null primary key identity,
--	AtlasCompanyId				uniqueidentifier	not null,
--	SFDCId						nvarchar (36)		not null,
--	CitwebId					int					not null
--)
--go