-- DROP TABLE dbo.Migration_Exception_Report_Document
CREATE TABLE dbo.Migration_Exception_Report_Document (
	Id							int					not null	PRIMARY KEY IDENTITY,
	ExceptionBatchCompanyId		int					not null,
	CitwebId					int					not null,
	AtlasCompanyId				uniqueidentifier	not null,
	AtlasIncidentId				int					not null,
	DocumentRootPath			varchar(500)		not null,
	DocumentName				varchar(100)		not null,
	InsertedOn					datetime			not null	DEFAULT GETDATE(),
)
