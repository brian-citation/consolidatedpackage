-- Input Params
DECLARE @p_CompanyId			uniqueidentifier = ? -- 'B668D1B8-CBE4-4CC5-82D6-AB0E6B463F16'
DECLARE @p_UserId				uniqueidentifier = ? -- '7A83762D-77CC-49E5-99F9-F763FEEE12FC'
DECLARE @p_AdviceCardNumber		varchar(20)		 = ? -- 'abcdEF3'
DECLARE @p_ServiceType			varchar(10)		 = ? -- 'BOTH'
--DECLARE 
--
-- Local Variables
DECLARE @AtlasAdviceCardId		uniqueidentifier
DECLARE @Now					smalldatetime			= getdate()
DECLARE @MappedServiceType		varchar(10)
--
-- Constants
DECLARE @c_DefaultUserId		uniqueidentifier = '89504E36-557B-4691-8F1B-7E86F9CF95EA'
--
SET @MappedServiceType = CASE WHEN @p_ServiceType = 'HS' THEN 'H&S' ELSE CASE WHEN @p_ServiceType = 'HR' THEN 'EL' ELSE @p_ServiceType END END
--
SELECT @AtlasAdviceCardId = Id FROM Citation.AdviceCards WHERE CompanyId = @p_CompanyId AND CardNumber = @p_AdviceCardNumber
--print @AtlasAdviceCardId
--
-- Get atlasadvicecard if exist or insert into Citation.AdviceCards and get the new id
IF @AtlasAdviceCardId IS NULL
BEGIN
	SET @AtlasAdviceCardId = NEWID()
	--
	INSERT INTO	Citation.AdviceCards (
				Id
				,CompanyId
				,CardNumber
				,CreatedOn
				,ModifiedOn
				,CreatedBy
				,ModifiedBy
				,IsArchived
				,IsDeleted
				,LCid
				,Version
				,IsActive) VALUES (
				 @AtlasAdviceCardId --Id
				,@p_CompanyId --CompanyId
				,@p_AdviceCardNumber --CardNumber
				,@Now --CreatedOn
				,@Now --ModifiedOn
				,@c_DefaultUserId --CreatedBy
				,@c_DefaultUserId --ModifiedBy
				,0 --IsArchived
				,0 --IsDeleted
				,1033 --LCid
				,'1.0' --Version
				,1 --IsActive
				)

--
END
--
-- insert into Citation.AdviceCardAssignments
IF NOT EXISTS (SELECT 'X' FROM Citation.AdviceCardAssignments WHERE UserId = @p_UserId AND AdviceCardId = @AtlasAdviceCardId)
INSERT INTO	Citation.AdviceCardAssignments (
			Id
			,UserId
			,AdviceCardId
			,CreatedOn
			,ModifiedOn
			,CreatedBy
			,ModifiedBy
			,IsDeleted
			,LCid
			,Version) VALUES (
			NEWID()--Id
			,@p_UserId --UserId
			,@AtlasAdviceCardId --AdviceCardId
			,@Now --CreatedOn
			,@Now --ModifiedOn
			,@c_DefaultUserId --CreatedBy
			,@c_DefaultUserId --ModifiedBy
			,0 --IsDeleted
			,1033 --LCid
			,'1.0' --Version
			)
--
-- Insert Citation.AdviceCardTypeMap if not exist for that card (won't insert anything if service type changes)
INSERT INTO	Citation.AdviceCardTypeMap (
			AdviceCardId
			,AdviceTypeId
			)
SELECT		@AtlasAdviceCardId
			,at.Id
FROM		Shared.AdviceTypes at
WHERE		(((@MappedServiceType = 'H&S' OR @MappedServiceType = 'BOTH') AND NAME = 'H&S')
		OR	((@MappedServiceType = 'EL' OR @MappedServiceType = 'BOTH') AND NAME = 'EL'))
AND	NOT EXISTS (SELECT 1 FROM Citation.AdviceCardTypeMap act WHERE act.AdviceCardId =  @AtlasAdviceCardId
)

