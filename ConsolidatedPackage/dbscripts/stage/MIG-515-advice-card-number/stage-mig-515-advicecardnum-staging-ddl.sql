-- DROP TABLE [dbo].[Shorthorn_Contacts_TotalDump]
CREATE TABLE [dbo].[Shorthorn_Contacts_TotalDump] (
    [contactID] int,
    [siteID] int,
    [SFDC_ContactId] nvarchar(50),
    [title] int,
    [fName] varchar(50),
    [mName] varchar(50),
    [sName] varchar(50),
    [position] varchar(500),
    [tel] varchar(200),
    [mob] varchar(200),
    [fax] varchar(200),
    [email] varchar(200),
    [notes] varchar(5000),
    [misc] varchar(500),
    [adviceCard] varchar(20),
    [adviceCardStatusContact] int,
    [adviceCardNotes] varchar(5000),
    [adviceCardUsed] int,
    [enabled] int,
    [ACCESS_SVID] int,
    [clientIDtemp] int
)