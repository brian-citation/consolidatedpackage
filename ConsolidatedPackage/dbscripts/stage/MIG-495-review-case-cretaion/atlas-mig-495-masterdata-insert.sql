DECLARE @hdCustomFields table (
	[FieldID] [int]  NOT NULL,
	[InstanceID] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[FieldName] [nvarchar](100) NOT NULL,
	[ForTechsOnly] [bit] NOT NULL,
	[Mandatory] [bit] NOT NULL,
	[ShowInGrid] [bit] NOT NULL,
	[OrderByNumber] [int] NOT NULL,
	[UsageType] [int] NOT NULL,
	[ParentCustomFieldId] [int] NULL,
	[ShowEmptyOption] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[SelectFirstOptionDefault] [bit] NULL
)
DECLARE @hdCustomFieldOptions TABLE(
	[OptionID] [int]  NOT NULL,
	[FieldID] [int] NOT NULL,
	[OptionValue] [nvarchar](500) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[ParentCustomFieldOptionId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[OrderByNumber] [int] NOT NULL
)
--
INSERT @hdCustomFields ([FieldID], [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]) VALUES (39, 0, 3, N'Campaign/Batch', 0, 0, 1, 22, 1, NULL, 1, 0, 0)
INSERT @hdCustomFields ([FieldID], [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]) VALUES (40, 0, 7, N'Closure Reason', 0, 0, 1, 25, 1, NULL, 1, 0, 0)
INSERT @hdCustomFields ([FieldID], [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]) VALUES (41, 0, 7, N'Closure SubTypes', 0, 0, 1, 26, 1, 40, 1, 0, 0)
INSERT @hdCustomFields ([FieldID], [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]) VALUES (42, 0, 1, N'Other Closure subtype', 0, 0, 1, 27, 1, NULL, 1, 0, 0)
INSERT @hdCustomFields ([FieldID], [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]) VALUES (43, 0, 4, N'Has Client Contacted?', 0, 0, 1, 28, 1, NULL, 1, 0, 0)
INSERT @hdCustomFields ([FieldID], [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]) VALUES (44, 0, 4, N'Has Client Logged In?', 0, 0, 1, 29, 1, NULL, 1, 0, 0)
INSERT @hdCustomFields ([FieldID], [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]) VALUES (45, 0, 4, N'Has Client Trained?', 0, 0, 1, 30, 1, NULL, 1, 0, 0)
--
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (548, 39, N'Trial-Inbound', 0, NULL, 0, 1)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (549, 39, N'Trial-Outbound', 0, NULL, 0, 2)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (550, 39, N'Batch-1', 0, NULL, 0, 3)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (551, 39, N'Batch-2', 0, NULL, 0, 4)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (552, 39, N'Batch-3', 0, NULL, 0, 5)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (553, 40, N'Successful', 0, NULL, 0, 1)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (554, 40, N'UnSuccessful', 0, NULL, 0, 2)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (555, 41, N'Client using system', 0, 1165, 0, 1)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (556, 41, N'Has existing system', 0, 1166, 0, 2)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (557, 41, N'Not interested', 0, 1166, 0, 3)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (558, 41, N'Terminating Contract', 0, 1166, 0, 4)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (559, 41, N'Complaint', 0, 1166, 0, 5)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (560, 41, N'Functionality gap', 0, 1166, 0, 6)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (561, 41, N'Other ( Please specify)', 0, 1166, 0, 7)
INSERT @hdCustomFieldOptions ([OptionID], [FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber]) VALUES (562, 27, N'Absence', 0, NULL, 0, 0)
--
--
INSERT INTO Incidents.hdCustomFields ([InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault])
SELECT [InstanceID], [Type], [FieldName], [ForTechsOnly], [Mandatory], [ShowInGrid], [OrderByNumber], [UsageType], [ParentCustomFieldId], [ShowEmptyOption], [IsDeleted], [SelectFirstOptionDefault]
FROM @hdCustomFields t
WHERE  t.fieldid IN(39,40,41,42,43,44,45)
AND NOT EXISTS (SELECT 'X' FROM Incidents.hdCustomFields t1 WHERE t1.Type = t.Type AND t1.FieldName = t.FieldName)
--
INSERT INTO Incidents.hdCustomFieldOptions ([FieldID], [OptionValue], [IsDefault], [ParentCustomFieldOptionId], [IsDeleted], [OrderByNumber])
SELECT	t2.NewFieldId, t.[OptionValue], t.[IsDefault], t.[ParentCustomFieldOptionId], t.[IsDeleted], t.[OrderByNumber]
FROM	@hdCustomFieldOptions t
INNER JOIN (SELECT tcf.FieldId OldFieldId, cf.FieldId NewFieldId, cf.FieldName
			FROM Incidents.hdCustomFields cf
			INNER JOIN @hdCustomFields tcf
			ON		tcf.Type		= cf.Type
			AND		tcf.FieldName	= cf.FieldName) t2
ON			t2.OldFieldId = t.FieldId
WHERE	t.fieldid IN(39,40,41,42,43,44,45)
AND NOT EXISTS (SELECT 'X' FROM Incidents.hdCustomFieldOptions t1 WHERE t1.FieldId = t.FieldId AND t1.OptionValue = t.OptionValue)
--
