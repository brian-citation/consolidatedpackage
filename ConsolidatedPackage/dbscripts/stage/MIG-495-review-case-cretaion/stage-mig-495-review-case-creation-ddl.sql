-- DROP TABLE  [Citation_LastLogins]
CREATE TABLE Citation_LastLogins (
    compID							INT					NOT NULL,
    LastLogin						DATETIME			NOT NULL
)
--
ALTER TABLE dbo.Migration_Exception_Report_Document ADD AtlasDocumentId UNIQUEIDENTIFIER NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000'
--
ALTER TABLE dbo.Migration_Exception_Report_Document ADD IsProcessed TINYINT NOT NULL DEFAULT 0
--
ALTER TABLE dbo.Stage_AdhocCompany ADD TrialGroup		VARCHAR(50) NOT NULL DEFAULT 'NotGiven'
ALTER TABLE dbo.Stage_AdhocCompany ADD Campaign			VARCHAR(50) NOT NULL DEFAULT 'NotGiven'
--
