DECLARE @p_AtlasCompanyId			uniqueidentifier	= ? -- '153898d7-c5ed-48c1-97e1-62c934332d47'
DECLARE @p_AtlasDocumentId			uniqueidentifier	= ? -- 'aaaaaaaa-c5ed-48c1-97e1-62c934332d47'
DECLARE @p_CitwebLastLogin			varchar(100)		= ? -- getdate()
DECLARE @p_CaseType					varchar(100)		= ? -- 'In-bound'
DECLARE @p_Campagin					varchar(100)		= ? -- 'Trail-Inbound'
DECLARE	@p_AtlasSiteURL				varchar(100)		= ? -- 'https://www.citation.co.uk'
--
DECLARE @NewIssueId					int
DECLARE @ServiceOwenerUserId		uniqueidentifier
DECLARE @JitBitUserId				int
DECLARE @CompanyName				nvarchar(1000)
DECLARE @Segment					nvarchar(20)
DECLARE @ServicesTaken				nvarchar(100) = ''
DECLARE @TenantName					nvarchar(100)
DECLARE @HQSiteId					uniqueidentifier
DECLARE @HQPostCode					nvarchar(100)
DECLARE @SitesCount					int
DECLARE @EmployeeCount				int
DECLARE @ConsultantName				nvarchar(200)
DECLARE @SOName						nvarchar(200)
DECLARE @SOEmail					nvarchar(200)
DECLARE @SOPhone					nvarchar(200)
DECLARE @Subject					nvarchar(2000)
DECLARE @Body						nvarchar(4000)
DECLARE @CaseTypeId					int
DECLARE @CaseTypeOptionId			int
DECLARE @CampaignId					int
DECLARE @CampaignOptionId			int
DECLARE	@ErrorMsg					varchar(2000)
--
DECLARE @C_NewLine					char(1) = char(10)
-- Get Company Name, Segment, Main Site Id, PostCode, TenantName
SELECT		@CompanyName = c.FullName, @Segment = cs.Title, @HQSiteId = s.Id, @HQPostCode = a.PostCode, @TenantName = c.TenantName
FROM		citation.Companies c
LEFT JOIN	dbo.Sites s
ON			s.CompanyId			= c.Id
LEFT JOIN	Citation.ClientSegments cs
ON			c.ClientSegmentId	= cs.Id
LEFT JOIN	Addresses a
ON			s.AddressId			= a.Id
WHERE		c.Id				= @p_AtlasCompanyId
AND			s.IsDeleted			= 0
AND			s.IsHeadOffice		= 1
--
-- Get Services Taken
SELECT		@ServicesTaken = @ServicesTaken + ClientServiceType + ','
FROM		(
SELECT		DISTINCT c.Id CompanyId, c.FullName,
			CASE WHEN cst.CitationServiceType = 1 THEN 'H&S' ELSE
					CASE WHEN cst.CitationServiceType = 2 THEN 'PEL' ELSE
							CASE WHEN cst.CitationServiceType = 3 THEN 'ISO' ELSE 'UNKNOWN' END END END ClientServiceType
FROM		Citation.Companies c
INNER JOIN	Citation.CompanyClientTypes cct
ON			cct.CompanyId		= c.Id
INNER JOIN	Shared.CitationServiceClientType cst
ON			cst.clienttypeid	= cct.ClientTypeId
WHERE		c.Id				= @p_AtlasCompanyId
) t
--
IF ISNULL(@ServicesTaken,'') <> ''
	SET @ServicesTaken = SUBSTRING(@ServicesTaken,1,LEN(@ServicesTaken)-1)
--
-- Build Subject
--
SET @Subject	= @CompanyName + ' - (' + ISNULL(@Segment,'') + ' / ' + ISNULL(@ServicesTaken,'') + ') - ' + @p_CaseType + ' - ' + FORMAT (getdate(), 'D', 'en-gb')
--
-- Site Count
SELECT @SitesCount = COUNT(*) FROM dbo.Sites WHERE CompanyId = @p_AtlasCompanyId AND IsDeleted = 0
-- Empoloyee Count
SELECT @EmployeeCount = COUNT(*) FROM dbo.Employees WHERE CompanyId = @p_AtlasCompanyId AND IsDeleted = 0
--
-- Get one of multiple service owners (active user gets priority)
SELECT		TOP 1 @ServiceOwenerUserId = u.Id, @JitBitUserId = JitbitUserId, @SOName = ISNULL(u.FirstName,'') + ' ' + ISNULL(u.SecondName,''),
			@SOEmail = u.Email, @SOPhone = ISNULL(COALESCE(u.Telephone, u.MobileNumber),'')
FROM		shared.Users u
WHERE		u.CompanyId			= @p_AtlasCompanyId
AND			u.IsDeleted			= 0
AND			u.IsActive			= 1
AND	EXISTS (SELECT 1
			FROM		UserProfilesMap upm
			WHERE		upm.UserId = u.id
			AND			upm.UserProfileId IN ('1078A9A4-101E-4B22-AF96-446AA7C5C793','ED8F7D9B-DFFD-49EC-8934-5327D2BF1AA4','EFD4D143-48C1-471C-B8BA-B91ADBC9B156'))
AND			u.IsDeleted			= 0
ORDER BY	u.IsActive, u.JitbitUserId DESC
--
-- Get UserId from incidents.hdUsers using email when jitbit is not available
--
IF (@JitBitUserId IS NULL)
	SELECT	TOP 1 @JitBitUserId = UserId FROM incidents.hdUsers WHERE Email = (SELECT Email FROM shared.Users WHERE Id = @ServiceOwenerUserId)
--
-- Get Consulttant
SELECT	@ConsultantName = ISNULL(u.FirstName,'') + ' ' + ISNULL(u.SecondName,'')
FROM	Shared.Users u
INNER JOIN Shared.SiteAssignments sa
ON		sa.UserId		= u.Id
INNER JOIN dbo.Sites s
ON		s.Id			= sa.SiteId
WHERE	s.CompanyId		= @p_AtlasCompanyId
AND		s.IsHeadOffice	= 1
AND		s.IsDeleted		= 0
AND		u.IsActive		= 1
AND		u.IsDeleted		= 0
--

SET @Body = @CompanyName + ', ' + @HQPostCode + @C_NewLine +
			'Tota Sites: ' + CAST(@SitesCount as varchar(3)) + ' Total Employees: ' + CAST(@EmployeeCount AS VARCHAR(5)) + @C_NewLine +
			'Consultant: ' + ISNULL(@ConsultantName, '') + @C_NewLine +
			'Last CitWeb log-in date: ' + @p_CitwebLastLogin + @C_NewLine + @C_NewLine +
			'Service Owner:  ' + ISNULL(@SOName,'') + ', email: ' + ISNULL(@SOEmail,'') + ', tel: ' + ISNULL(@SOPhone,'') + @C_NewLine +
			'Exception report: ' + @p_AtlasSiteURL + '/#/document/' + CAST(@p_AtlasDocumentId AS VARCHAR(36)) + '?cid=' + CAST(@p_AtlasCompanyId AS VARCHAR(36)) + @C_NewLine +
			'Company details page: ' + @p_AtlasSiteURL + '/#/company/' + CAST(@p_AtlasCompanyId AS VARCHAR(36)) + '?cid=' + CAST(@p_AtlasCompanyId AS VARCHAR(36))
-- Raio
IF @JitBitUserId IS NULL
	SET @JitBitUserId = 6795

--	SET @ErrorMsg = 'Unable to get UserId of Service Swner for the company ' + @CompanyName
--	RAISERROR(@ErrorMsg, 16, 1)
INSERT INTO incidents.hdIssues (
	InstanceID
	,IssueDate
	,UserID
	,CategoryID
	,Subject
	,Body
	,UpdatedByUser
	,UpdatedByPerformer
	,UpdatedForTechView
	,StatusID
	,PublishToKB
	,Priority
	,TimeSpentInSeconds
	,KBForTechsOnly
	,LastUpdated
	,ChannelId
	,IsDeleted
	,IsAdviceGuaranteed

) VALUES (
	0 -- InstanceID
	,GETDATE() -- IssueDate
	,@JitBitUserId -- UserID
	,(SELECT CategoryId FROM incidents.hdcategories WHERE Name = 'Migration') -- CategoryID
	,@Subject -- Subject
	,@Body -- Body
	,0 -- UpdatedByUser
	,0 -- UpdatedByPerformer
	,0 -- UpdatedForTechView
	,1 -- StatusID
	,0 -- PublishToKB
	,0 -- Priority
	,0 -- TimeSpentInSeconds
	,0 -- KBForTechsOnly
	,GETDATE() -- LastUpdated
	,1 -- ChannelId
	,0 -- IsDeleted
	,0 -- IsAdviceGuaranteed
)
--
SET @NewIssueId = SCOPE_IDENTITY()
--
-- 
IF (LEN(@p_CaseType) > 0)
BEGIN
	--
	SELECT @CaseTypeId = FieldId FROM Incidents.hdCustomFields WHERE FieldName = 'Case Type'
	--
	IF (@CaseTypeId IS NOT NULL)
	BEGIN
		SELECT @CaseTypeOptionId = OptionId FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CaseTypeId AND OptionValue = @p_CaseType AND IsDeleted = 0
		-- Create option if not exists
		--
		IF (@CaseTypeOptionId IS NULL)
		BEGIN
			INSERT INTO Incidents.hdCustomFieldOptions (FieldId, OptionValue, IsDefault, IsDeleted, OrderByNumber)
			VALUES
			(@CaseTypeId, @p_CaseType, 0, 0, (SELECT MAX(OrderByNumber) + 1 FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CaseTypeId))
			--
			SET @CaseTypeOptionId = SCOPE_IDENTITY()
		END
		--
		INSERT INTO	Incidents.hdCustomFieldValues (IssueID, FieldID, Value) VALUES (@NewIssueId, @CaseTypeId, @CaseTypeOptionId)
		--
	END
END
--
IF (LEN(@p_Campagin) > 0)
BEGIN
	--
	SELECT @CampaignId = FieldId FROM Incidents.hdCustomFields WHERE FieldName = 'Campaign/Batch'
	--
	IF (@CampaignId IS NOT NULL)
	BEGIN
		SELECT @CampaignOptionId = OptionId FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CampaignId AND OptionValue = @p_Campagin AND IsDeleted = 0
		-- Create option if not exists
		--
		IF (@CampaignOptionId IS NULL)
		BEGIN
			INSERT INTO Incidents.hdCustomFieldOptions (FieldId, OptionValue, IsDefault, IsDeleted, OrderByNumber)
			VALUES
			(@CampaignId, @p_Campagin, 0, 0, (SELECT MAX(OrderByNumber) + 1 FROM Incidents.hdCustomFieldOptions WHERE FieldId = @CampaignId))
			--
			SET @CampaignOptionId = SCOPE_IDENTITY()
		END
		--
		INSERT INTO	Incidents.hdCustomFieldValues (IssueID, FieldID, Value) VALUES (@NewIssueId, @CampaignId, @CampaignOptionId)
	END
END
--
SELECT ? = @NewIssueId, ? = @TenantName
--
