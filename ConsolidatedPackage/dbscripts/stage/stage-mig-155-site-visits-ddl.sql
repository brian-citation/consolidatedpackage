-- Stage Table
-- Drop table [dbo].[Citweb_citdocsobservations]
CREATE TABLE dbo.Citweb_citdocsobservations(
	recordid				int					NOT NULL,
	companyuid				int					NULL,
	sitename				varchar(max)		NULL,
	obsno					varchar(max)		NULL,
	observation				varchar(max)		NULL,
	recomendation			varchar(max)		NULL,
	responsibility			varchar(max)		NULL,
	completed				datetime			NULL,
	priority				varchar(max)		NULL,
	senddatetime			datetime			NULL,
	obsID					int					NULL,
	priorityInt				int					NULL,
	deadline				datetime			NULL,
	documentdate			datetime			NULL,
	visitdate				datetime			NULL,
	rectcost				money				NULL,
	filename				varchar(max)		NULL,
	obscat					varchar(max)		NULL,
	revision				int					NOT NULL,
	email					varchar(max)		NULL,
	emailSent				int					NOT NULL,
	percentcomp				int					NULL,
	outstanding				tinyint				NOT NULL,
	lastObsEmail			datetime			NULL,
 CONSTRAINT PK_citdocsobservations PRIMARY KEY CLUSTERED 
	(
		recordid ASC
	)
)
GO
--
-- Mapping Table
-- DROP TABLE dbo.Stage_SiteVisits_Track
CREATE TABLE dbo.Stage_SiteVisits_Track (
	Id						int					NOT NULL PRIMARY KEY IDENTITY,
	CitwebCompanyId			int					NOT NULL,
	AtlasCompanyId			uniqueidentifier	NOT NULL,
	CitwebSiteId			int					NOT NULL,
	AtlasSiteId				uniqueidentifier	NOT NULL,
	VisitDate				datetime			NOT NULL,
	FileName				varchar(max)		NOT NULL,
	AtlasSiteVisitsId		uniqueidentifier	NOT NULL,
	CreatedOn				datetime			NOT NULL DEFAULT GETDATE()
)
--
