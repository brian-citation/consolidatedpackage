
CREATE function [dbo].[FN_GetAtlastCountyId](@County varchar(100), @postcode varchar(50)) returns uniqueidentifier
with SCHEMABINDING
 as 
 begin
    
	Declare @I_CountyID uniqueidentifier
	Declare @MCountyId varchar(50)

	select @I_CountyID=ID from Atlas_Counties_Dump where Name=@County

	--print 1 
	IF isnull(@County,'')='' or @I_CountyID is null
	BEGIN
	--print 2
		SELECT @MCountyId=[County]  FROM [dbo].[UK_PostCodes_Mapping] where Postcode=@postcode
	END

	IF isnull(@MCountyId,'')='' 
	BEGIN	
		SELECT @MCountyId=[County]  FROM [dbo].[UK_PostCodes_Mapping] where PostCode4Chars=left(replace(@postcode,' ',''),4)
	END

	IF isnull(@MCountyId,'')='' 
	BEGIN	
		SELECT @MCountyId=[County]  FROM [dbo].[UK_PostCodes_Mapping] where PostCode3Chars=left(replace(@postcode,' ',''),3)
	END
	IF isnull(@MCountyId,'')='' 
	BEGIN	
		SELECT @MCountyId=[County]  FROM [dbo].[UK_PostCodes_Mapping] where PostCode2Chars=left(replace(@postcode,' ',''),2)
	END
	--print 3
	select @I_CountyID=ID from Atlas_Counties_Dump where Name=COALESCE(@MCountyId,@County)

	
    return @I_CountyID
end


GO


