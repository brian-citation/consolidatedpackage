drop function [dbo].[FN_GetAtlastCountryId]
go
--
create  function [dbo].[FN_GetAtlastCountryId](@country varchar(100), @postcode varchar(50)) returns uniqueidentifier
with SCHEMABINDING
 as 
 begin
    
	Declare @I_CountryID uniqueidentifier
	declare @MappingCountryName varchar(100);


	IF (@country='Elgland' OR @country='England' OR @country='Enlgand' OR @country='uk'OR @country='United Kingdom'OR @country='United Kingdon')
	BEGIN
		SET @country='England'
	END

	select @I_CountryID=ID from dbo.Atlas_Countries_Dump where Name=@country

	--print 1 
	IF isnull(@country,'')='' or @I_CountryID is null
	BEGIN
	--print 2
		SELECT @MappingCountryName=[Country]  FROM [dbo].[UK_PostCodes_Mapping] where Postcode=@postcode
	END

	IF isnull(@MappingCountryName,'')='' 
	BEGIN	
		SELECT @MappingCountryName=[Country]  FROM [dbo].[UK_PostCodes_Mapping] where PostCode4Chars=left(replace(@postcode,' ',''),4)
	END

	IF isnull(@MappingCountryName,'')='' 
	BEGIN	
		SELECT @MappingCountryName=[Country]  FROM [dbo].[UK_PostCodes_Mapping] where PostCode3Chars=left(replace(@postcode,' ',''),3)
	END
	IF isnull(@MappingCountryName,'')='' 
	BEGIN	
		SELECT @MappingCountryName=[Country]  FROM [dbo].[UK_PostCodes_Mapping] where PostCode2Chars=left(replace(@postcode,' ',''),2)
	END
	--print 3
	select @I_CountryID=ID from dbo.Atlas_Countries_Dump where Name=COALESCE(@MappingCountryName,@country)

    return @I_CountryID
end
